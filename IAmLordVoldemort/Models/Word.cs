﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IAmLordVoldemort.Models
{
    public class Word
    {

        public int Id { get; set; }

        public string Value { get; set; }

        public byte A { get; set; }
        public byte B { get; set; }
        public byte C { get; set; }
        public byte D { get; set; }
        public byte E { get; set; }
        public byte F { get; set; }
        public byte G { get; set; }
        public byte H { get; set; }
        public byte I { get; set; }
        public byte J { get; set; }
        public byte K { get; set; }
        public byte L { get; set; }
        public byte M { get; set; }
        public byte N { get; set; }
        public byte O { get; set; }
        public byte P { get; set; }
        public byte Q { get; set; }
        public byte R { get; set; }
        public byte S { get; set; }
        public byte T { get; set; }
        public byte U { get; set; }
        public byte V { get; set; }
        public byte W { get; set; }
        public byte X { get; set; }
        public byte Y { get; set; }
        public byte Z { get; set; }


        public Word()
        {
        }

        public Word(string value, int? id = null)
        {
            Value = value;
            if (id != null)
                Id = id.Value;
            CalculateLetters();
        }


        public void CalculateLetters()
        {
            string lowerValue = Value.ToLower();

            A = Convert.ToByte(lowerValue.Count(x => x == 'a'));
            B = Convert.ToByte(lowerValue.Count(x => x == 'b'));
            C = Convert.ToByte(lowerValue.Count(x => x == 'c'));
            D = Convert.ToByte(lowerValue.Count(x => x == 'd'));
            E = Convert.ToByte(lowerValue.Count(x => x == 'e'));
            F = Convert.ToByte(lowerValue.Count(x => x == 'f'));
            G = Convert.ToByte(lowerValue.Count(x => x == 'g'));
            H = Convert.ToByte(lowerValue.Count(x => x == 'h'));
            I = Convert.ToByte(lowerValue.Count(x => x == 'i'));
            J = Convert.ToByte(lowerValue.Count(x => x == 'j'));
            K = Convert.ToByte(lowerValue.Count(x => x == 'k'));
            L = Convert.ToByte(lowerValue.Count(x => x == 'l'));
            M = Convert.ToByte(lowerValue.Count(x => x == 'm'));
            N = Convert.ToByte(lowerValue.Count(x => x == 'n'));
            O = Convert.ToByte(lowerValue.Count(x => x == 'o'));
            P = Convert.ToByte(lowerValue.Count(x => x == 'p'));
            Q = Convert.ToByte(lowerValue.Count(x => x == 'q'));
            R = Convert.ToByte(lowerValue.Count(x => x == 'r'));
            S = Convert.ToByte(lowerValue.Count(x => x == 's'));
            T = Convert.ToByte(lowerValue.Count(x => x == 't'));
            U = Convert.ToByte(lowerValue.Count(x => x == 'u'));
            V = Convert.ToByte(lowerValue.Count(x => x == 'v'));
            W = Convert.ToByte(lowerValue.Count(x => x == 'w'));
            X = Convert.ToByte(lowerValue.Count(x => x == 'x'));
            Y = Convert.ToByte(lowerValue.Count(x => x == 'y'));
            Z = Convert.ToByte(lowerValue.Count(x => x == 'z'));
        }


        public override string ToString()
        {
            return Value;
        }

    }
}
