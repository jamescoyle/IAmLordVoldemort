﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IAmLordVoldemort.Models
{
    public class IalvContext : DbContext
    {

        public IalvContext(DbContextOptions<IalvContext> options)
            : base(options)
        {
        }

        public DbSet<Word> Words { get; set; }

    }
}
