﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace IAmLordVoldemort.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Words",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    A = table.Column<int>(nullable: false),
                    B = table.Column<int>(nullable: false),
                    C = table.Column<int>(nullable: false),
                    D = table.Column<int>(nullable: false),
                    E = table.Column<int>(nullable: false),
                    F = table.Column<int>(nullable: false),
                    G = table.Column<int>(nullable: false),
                    H = table.Column<int>(nullable: false),
                    I = table.Column<int>(nullable: false),
                    J = table.Column<int>(nullable: false),
                    K = table.Column<int>(nullable: false),
                    L = table.Column<int>(nullable: false),
                    M = table.Column<int>(nullable: false),
                    N = table.Column<int>(nullable: false),
                    O = table.Column<int>(nullable: false),
                    P = table.Column<int>(nullable: false),
                    Q = table.Column<int>(nullable: false),
                    R = table.Column<int>(nullable: false),
                    S = table.Column<int>(nullable: false),
                    T = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    U = table.Column<int>(nullable: false),
                    V = table.Column<int>(nullable: false),
                    Value = table.Column<string>(nullable: true),
                    W = table.Column<int>(nullable: false),
                    X = table.Column<int>(nullable: false),
                    Y = table.Column<int>(nullable: false),
                    Z = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Words", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Words");
        }
    }
}
