﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IAmLordVoldemort.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace IAmLordVoldemort.Pages
{
    public class updateModel : PageModel
    {

        private IalvContext _dbContext;

        public int UpdateCount { get; set; }

        
        public updateModel(IalvContext dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<IActionResult> OnGetAsync()
        {
            try
            {
                UpdateCount = 0;

                string filePath = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "wwwroot", "words.txt");

                if (System.IO.File.Exists(filePath))
                {
                    string[] lines = await System.IO.File.ReadAllLinesAsync(filePath);
                    foreach (string line in lines)
                    {
                        string trimmedLine = line.Trim().ToLower();
                        if (string.IsNullOrWhiteSpace(trimmedLine))
                            continue;

                        Word word = await (from x in _dbContext.Words
                                           where
                                               x.Value == trimmedLine
                                           select new Word() { Id = x.Id, Value = x.Value }
                        ).SingleOrDefaultAsync();

                        if (word == null)
                        {
                            word = new Word(trimmedLine);
                            await _dbContext.AddAsync(word);
                            UpdateCount++;
                        }
                        if (UpdateCount > 0)
                            await _dbContext.SaveChangesAsync();
                    }

                    System.IO.File.Delete(filePath);
                }
            }
            catch
            {
            }

            return Page();
        }
    }
}