﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using IAmLordVoldemort.Models;
using Microsoft.EntityFrameworkCore;

namespace IAmLordVoldemort.Pages
{
    public class indexModel : PageModel
    {

        private IalvContext _dbContext;


        [BindProperty]
        public string MuggleName { get; set; }

        [BindProperty]
        public string RequiredText { get; set; }

        public List<string> Sentences { get; set; }

        public string ErrorMessage { get; set; }

        private Random Random = new Random();


        private bool? _isMobile = null;
        public bool IsMobile
        {
            get {
                if (_isMobile == null)
                    _isMobile = Utils.IsMobileBrowser(Request);
                return _isMobile.Value;
            }
        }


        public indexModel(IalvContext dbContext)
        {
            _dbContext = dbContext;
        }


        public void OnGet()
        {
        }


        public async Task<IActionResult> OnPostAsync()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(MuggleName))
                {
                    ErrorMessage = "Please, try not to be embarrassed of your disgusting muggle heritage. Enter your name.";
                    return Page();
                }

                string lowerMuggleName = MuggleName.ToLower();
                RequiredText = RequiredText ?? string.Empty;
                RequiredText = RequiredText.Trim();
                string lowerRequiredText = RequiredText.ToLower();
                lowerRequiredText = string.Join("", lowerRequiredText.Where(x => char.IsLetter(x)));
                foreach (char letter in lowerRequiredText)
                {
                    int muggleNameIndex = lowerMuggleName.IndexOf(letter);
                    if (muggleNameIndex < 0)
                    {
                        ErrorMessage = $"You don't have enough {letter.ToString().ToUpper()}'s in your name for {RequiredText}";
                        return Page();
                    }
                    lowerMuggleName = lowerMuggleName.Remove(muggleNameIndex, 1);
                }

                Word target = new Word(lowerMuggleName);

                List<Word> words = await (from x in _dbContext.Words
                    where
                        x.A <= target.A && x.B <= target.B && x.C <= target.C && x.D <= target.D && x.E <= target.E &&
                        x.F <= target.F && x.G <= target.G && x.H <= target.H && x.I <= target.I && x.J <= target.J &&
                        x.K <= target.K && x.L <= target.L && x.M <= target.M && x.N <= target.N && x.O <= target.O &&
                        x.P <= target.P && x.Q <= target.Q && x.R <= target.R && x.S <= target.S && x.T <= target.T &&
                        x.U <= target.U && x.V <= target.V && x.W <= target.W && x.X <= target.X && x.Y <= target.Y &&
                        x.Z <= target.Z
                    select new Word() { Id = x.Id, Value = x.Value }
                ).ToListAsync();

                words.ForEach(x => x.CalculateLetters());

                List<List<Word>> sentences = MakeUnorderedSentences(words, target);
                if (!string.IsNullOrWhiteSpace(RequiredText))
                {
                    foreach (var sentence in sentences)
                        sentence.Add(new Word() { Value = RequiredText });
                }
                sentences = RandomiseSentences(sentences);

                Sentences = new List<string>();
                foreach (List<Word> sentenceList in sentences)
                    Sentences.Add(string.Join(' ', sentenceList.Select(x => x.Value)));

                return Page();
            }
            catch(Exception ex)
            {
                ErrorMessage = $"Something went really wrong: {ex.Message} - {ex.StackTrace}";
                return Page();
            }
        }


        private List<List<Word>> RandomiseSentences(List<List<Word>> sentences)
        {
            List<List<Word>> output = new List<List<Word>>();
            foreach(List<Word> sentence in sentences)
            {
                List<Word> newSentence = new List<Word>();
                while (true)
                {
                    List<Word> oldSentence = sentence.ToList();

                    while (oldSentence.Count > 0)
                    {
                        int wordIndex = Random.Next(oldSentence.Count);
                        Word word = oldSentence[wordIndex];
                        oldSentence.RemoveAt(wordIndex);
                        newSentence.Add(word);
                    }

                    if (newSentence.Last().Value == "a" && newSentence.Any(x => x.Value != "a"))
                        newSentence = new List<Word>();
                    else
                        break;
                }

                output.Add(newSentence);
            }
            return output;
        }


        private List<List<Word>> MakeUnorderedSentences(List<Word> words, Word targetWord, int returnCount = 8, int maxTryCount = 10000)
        {
            words = words.OrderByDescending(x => x.Value.Length).ToList();

            List<List<Word>> output = new List<List<Word>>();
            if (words.Count == 0)
                return output;
            
            for(int i = 0; i < maxTryCount; i++)
            {
                Word word = words[Random.Next(0, words.Count)];
                List<Word> sentence = new List<Word>() { word };
                sentence = FinishSentence(words, 0, targetWord, sentence);

                if(sentence != null)
                {
                    bool sentenceAlreadyExists = false;
                    foreach (List<Word> existingSentence in output)
                    {
                        if (existingSentence.All(x => sentence.Any(y => y.Value == x.Value)))
                            sentenceAlreadyExists = true;
                    }

                    if (!sentenceAlreadyExists)
                    {
                        output.Add(sentence);
                        if (output.Count >= returnCount)
                            return output;
                    }
                }
            }
            return output;
        }


        private List<Word> FinishSentence(List<Word> words, int index, Word targetWord, List<Word> currentSentence)
        {
            Word combined = new Word();
            foreach (Word word in currentSentence)
                combined.Value += word.Value;
            combined.CalculateLetters();

            if (combined.A == targetWord.A &&
                combined.B == targetWord.B &&
                combined.C == targetWord.C &&
                combined.D == targetWord.D &&
                combined.E == targetWord.E &&
                combined.F == targetWord.F &&
                combined.G == targetWord.G &&
                combined.H == targetWord.H &&
                combined.I == targetWord.I &&
                combined.J == targetWord.J &&
                combined.K == targetWord.K &&
                combined.L == targetWord.L &&
                combined.M == targetWord.M &&
                combined.N == targetWord.N &&
                combined.O == targetWord.O &&
                combined.P == targetWord.P &&
                combined.Q == targetWord.Q &&
                combined.R == targetWord.R &&
                combined.S == targetWord.S &&
                combined.T == targetWord.T &&
                combined.U == targetWord.U &&
                combined.V == targetWord.V &&
                combined.W == targetWord.W &&
                combined.X == targetWord.X &&
                combined.Y == targetWord.Y &&
                combined.Z == targetWord.Z)
            {
                return currentSentence;
            }

            for (int i = index; i < words.Count; i++)
            {
                //Randomly skip out words to prevent one word from determining everything that will follow
                if (Random.NextDouble() < 0.2)
                    continue;

                Word word = words[i];

                if (word.A + combined.A <= targetWord.A &&
                    word.B + combined.B <= targetWord.B &&
                    word.C + combined.C <= targetWord.C &&
                    word.D + combined.D <= targetWord.D &&
                    word.E + combined.E <= targetWord.E &&
                    word.F + combined.F <= targetWord.F &&
                    word.G + combined.G <= targetWord.G &&
                    word.H + combined.H <= targetWord.H &&
                    word.I + combined.I <= targetWord.I &&
                    word.J + combined.J <= targetWord.J &&
                    word.K + combined.K <= targetWord.K &&
                    word.L + combined.L <= targetWord.L &&
                    word.M + combined.M <= targetWord.M &&
                    word.N + combined.N <= targetWord.N &&
                    word.O + combined.O <= targetWord.O &&
                    word.P + combined.P <= targetWord.P &&
                    word.Q + combined.Q <= targetWord.Q &&
                    word.R + combined.R <= targetWord.R &&
                    word.S + combined.S <= targetWord.S &&
                    word.T + combined.T <= targetWord.T &&
                    word.U + combined.U <= targetWord.U &&
                    word.V + combined.V <= targetWord.V &&
                    word.W + combined.W <= targetWord.W &&
                    word.X + combined.X <= targetWord.X &&
                    word.Y + combined.Y <= targetWord.Y &&
                    word.Z + combined.Z <= targetWord.Z)
                {
                    currentSentence.Add(word);

                    List<Word> finishedSentence = FinishSentence(words, i + 1, targetWord, currentSentence.ToList());
                    if (finishedSentence == null)
                        return null;

                    return finishedSentence;
                }
            }

            return null;
        }
    }
}