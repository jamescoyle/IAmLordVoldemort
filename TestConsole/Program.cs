﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace TestConsole
{
    class Program
    {
        private static string ConnectionString =
            @"Server=(localdb)\mssqllocaldb;Database=IAmLordVoldemort;Trusted_Connection=True;ConnectRetryCount=0";

        static void Main(string[] args)
        {
            string filename = @"C:\Users\james\Desktop\harrypotter.txt";
            //ProcessWordFile(filename, dbLookup: true);
            AddWords(filename);
        }



        private static void ProcessWordFile(string filename, bool dbLookup)
        {
            string text = System.IO.File.ReadAllText(filename);
            text = text.Replace("\"", " ");
            text = text.Replace(",", " ");
            text = text.Replace(".", " ");
            text = text.Replace(":", " ");
            text = text.Replace(";", " ");
            text = text.Replace("-", " ");
            text = text.Replace("!", " ");
            text = text.Replace("(", " ");
            text = text.Replace(")", " ");
            text = text.Replace("?", " ");
            text = text.Replace(" '", " ");
            text = text.Replace("' ", " ");
            text = text.Replace("  ", " ");
            List<string> words = text.Split(new string[] { " ", Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();
            words = words.Distinct().ToList();
            words = words.Select(x => x.Trim().ToLower()).ToList();
            words = words.OrderBy(x => x).ToList();

            if (dbLookup)
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();

                    foreach (string word in words.ToList())
                    {
                        Console.WriteLine(word);
                        string query = "SELECT COUNT(Id) FROM Words WHERE LOWER(Value) = @param;";

                        SqlCommand command = new SqlCommand(query, connection);
                        command.Parameters.Add(new SqlParameter("@param", word.ToLower()));

                        if ((int)command.ExecuteScalar() > 0)
                            words.Remove(word);
                    }
                }
            }

            System.IO.File.WriteAllLines(filename, words.ToArray());
        }



        private static void AddWords(string filename)
        {
            string[] words = System.IO.File.ReadAllLines(filename);

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                
                char[] letters = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

                foreach (string word in words)
                {
                    StringBuilder stb = new StringBuilder("INSERT INTO Words (Value");

                    foreach (char letter in letters)
                        stb.Append(',').Append(letter);

                    stb.Append(") VALUES (@param");

                    string upperWord = word.ToUpper();
                    foreach (char letter in letters)
                        stb.Append(',').Append(upperWord.Count(x => x == letter));

                    stb.Append(")");

                    SqlCommand command = new SqlCommand(stb.ToString(), connection);
                    command.Parameters.Add(new SqlParameter("@param", word));

                    command.ExecuteNonQuery();

                    Console.WriteLine(word);
                }
            }

            Console.ReadKey();
        }



        private static void GetWordTypes()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                List<Tuple<int, string>> words = new List<Tuple<int, string>>();

                using (SqlDataReader reader = new SqlCommand("SELECT Id, Value FROM Words WHERE Type IS NULL", connection).ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int id = Convert.ToInt32(reader[0]);
                        string value = reader[1].ToString();

                        words.Add(new Tuple<int, string>(id, value));
                    }
                }

                HtmlWeb web = new HtmlWeb();

                foreach (Tuple<int, string> word in words)
                {
                    try
                    {
                        HtmlDocument doc = web.Load($"https://www.merriam-webster.com/dictionary/{word.Item2.ToLower()}");
                        HtmlNode node = doc.DocumentNode.SelectSingleNode("//span[@class='fl']");
                        if (node != null)
                        {
                            string wordClass = node.InnerText;
                            new SqlCommand($"UPDATE Words SET Type = '{wordClass}' WHERE Id = {word.Item1}", connection).ExecuteNonQuery();
                            Console.WriteLine(word.Item2.PadRight(20, ' ') + wordClass);
                        }
                        else
                            Console.WriteLine(word.Item2.PadRight(20, ' ') + "NOT FOUND");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error: {ex.Message}");
                    }
                }
            }
        }



        private static void ManualTypeUpdate()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                List<Tuple<int, string>> words = new List<Tuple<int, string>>();

                using (SqlDataReader reader = new SqlCommand("SELECT Id, Value FROM Words WHERE Type IS NULL", connection).ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int id = Convert.ToInt32(reader[0]);
                        string value = reader[1].ToString();

                        words.Add(new Tuple<int, string>(id, value));
                    }
                }

                foreach (Tuple<int, string> word in words)
                {
                    Console.WriteLine(word.Item2);
                    string wordType = Console.ReadLine();
                    if (!string.IsNullOrWhiteSpace(wordType))
                        new SqlCommand($"UPDATE Words SET Type = '{wordType}' WHERE Id = {word.Item1}", connection).ExecuteNonQuery();
                    Console.WriteLine();
                }
            }
        }
    }
}
